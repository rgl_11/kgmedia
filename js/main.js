$(document).ready(function() {

  if ($(window).load(function() {
        $("#preloader").fadeOut("slow", function() {
            $(this).remove()
        })
    })
  )

  $(".menu__icon").click(function() {
    $('.menu__show').addClass('open');
    $('.menu__show__overlay').addClass('open');
  });
  $(".menu__close").click(function() {
    $('.menu__show').removeClass('open');
    $('.menu__show__overlay').removeClass('open');
  });
  $(".menu__show__overlay").click(function() {
    $('.menu__show').removeClass('open');
    $('.menu__show__overlay').removeClass('open');
  });


  //slide 4 clik to play
  $(".section__4__overlayplay").click(function() {
    $(this).hide();
    $('.section__4__overlayplay__overlay').hide();
    player.playVideo();
  });
  $(".ytplayer").click(function() {
    player.playVideo();
  });
  $(".ytplayerb").click(function() {
    player.playVideo();
  });



   $('.section__slide').slick({
     slidesToShow: 1,
     slidesToScroll: 1,
     //autoplay: true,
     //autoplaySpeed: 2000,
     dots: true,
     arrows: false,
     infinite : false,
     //centerMode: true,
   });

   $('.section__slide').slick('setPosition');



   (function($){
        $(window).on("load",function(){
            $(".mCustomScrollbar").mCustomScrollbar({
              theme: "minimal-dark"
            });
            $(".mCustomScrollbar_x").mCustomScrollbar({
              axis:"x"
            });
        });
    })(jQuery);

  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

    // mobile
    //rotate icon
    var $lis = $('.icon_slide .icon_slide__item'),
        $lis_b = $('.section__2__footer__js li'),
         $cur = $lis.first().addClass('current'),
         $cur_b = $lis_b.first().addClass('current'),
         $next = $cur.next().addClass('next');
         $next_b = $cur_b.next().addClass('next');
         var timer = setInterval(function () {
             $cur.removeClass('current');
             $cur_b.removeClass('current');
             $cur = $next.removeClass('next').addClass('current');
             $cur_b = $next_b.removeClass('next').addClass('current');

             $next = $cur.next();
             $next_b = $cur_b.next();
             if (!$next.length) {
                 $next = $lis.first();
             }
             $next.addClass('next');

             if (!$next_b.length) {
                 $next_b = $lis_b.first();
             }
             $next_b.addClass('next');
         }, 2500);


         var $itemslide = $('.icon_slide .icon_slide__item');
         var $itemfooter = $('.section__2__footer__js ul li');

         $(".icon_slide__item, .section__2__footer__js li").click(function() {
           $itemslide.removeClass('current');
           clearInterval(timer);
         });

         $(".icon_slide__item__1, .itemfooter1").click(function() {
           $itemfooter.removeClass('current');
           $('.icon_slide__item__1').addClass('current');
           $('.itemfooter1').addClass('current');
         });
         $(".icon_slide__item__2, .itemfooter2").click(function() {
           $itemfooter.removeClass('current');
           $('.icon_slide__item__2').addClass('current');
           $('.itemfooter2').addClass('current');
         });
         $(".icon_slide__item__3, .itemfooter3").click(function() {
           $itemfooter.removeClass('current');
           $('.icon_slide__item__3').addClass('current');
           $('.itemfooter3').addClass('current');
         });
         $(".icon_slide__item__4, .itemfooter4").click(function() {
           $itemfooter.removeClass('current');
           $('.icon_slide__item__4').addClass('current');
           $('.itemfooter4').addClass('current');
         });
         $(".icon_slide__item__5, .itemfooter5").click(function() {
           $itemfooter.removeClass('current');
           $('.icon_slide__item__5').addClass('current');
           $('.itemfooter5').addClass('current');
         });
         $(".icon_slide__item__6, .itemfooter6").click(function() {
           $itemfooter.removeClass('current');
           $('.icon_slide__item__6').addClass('current');
           $('.itemfooter6').addClass('current');
         });


  } else {

    // desktop
    $('.section__slide').on('init', function(event, slick){
         //console.log('slider was initialized');
     })
     .on('beforeChange', function(event, slick, currentSlide, nextSlide){
         //console.log('before change');
         //playerb.playVideo();
         player.pauseVideo();
         playerb.pauseVideo();
     })
     .on('afterChange', function(event, slick, currentSlide, nextSlide){
       playerb.playVideo();
     });

    //rotate icon
    var $lis = $('.icon_slide .icon_slide__item'),
        $lis_b = $('.section__2__footer__js li'),
         $cur = $lis.first().addClass('current'),
         $cur_b = $lis_b.first().addClass('current'),
         $next = $cur.next().addClass('next');
         $next_b = $cur_b.next().addClass('next');
         var timer = setInterval(function () {
             $cur.removeClass('current');
             $cur_b.removeClass('current');
             $cur = $next.removeClass('next').addClass('current');
             $cur_b = $next_b.removeClass('next').addClass('current');

             $next = $cur.next();
             $next_b = $cur_b.next();
             if (!$next.length) {
                 $next = $lis.first();
             }
             $next.addClass('next');

             if (!$next_b.length) {
                 $next_b = $lis_b.first();
             }
             $next_b.addClass('next');
         }, 2500);


         var $itemslide = $('.icon_slide .icon_slide__item');
         var $itemfooter = $('.section__2__footer__js ul li');

         $(".icon_slide__item, .section__2__footer__js li").hover(function() {
           $itemslide.removeClass('current');
           clearInterval(timer);
         });

         $(".icon_slide__item__1, .itemfooter1").hover(function() {
           $itemfooter.removeClass('current');
           $('.icon_slide__item__1').addClass('current');
           $('.itemfooter1').addClass('current');
         });
         $(".icon_slide__item__2, .itemfooter2").hover(function() {
           $itemfooter.removeClass('current');
           $('.icon_slide__item__2').addClass('current');
           $('.itemfooter2').addClass('current');
         });
         $(".icon_slide__item__3, .itemfooter3").hover(function() {
           $itemfooter.removeClass('current');
           $('.icon_slide__item__3').addClass('current');
           $('.itemfooter3').addClass('current');
         });
         $(".icon_slide__item__4, .itemfooter4").hover(function() {
           $itemfooter.removeClass('current');
           $('.icon_slide__item__4').addClass('current');
           $('.itemfooter4').addClass('current');
         });
         $(".icon_slide__item__5, .itemfooter5").hover(function() {
           $itemfooter.removeClass('current');
           $('.icon_slide__item__5').addClass('current');
           $('.itemfooter5').addClass('current');
         });
         $(".icon_slide__item__6, .itemfooter6").hover(function() {
           $itemfooter.removeClass('current');
           $('.icon_slide__item__6').addClass('current');
           $('.itemfooter6').addClass('current');
         });


       }

       //btnback
       $('a[data-slide]').click(function(e) {
          e.preventDefault();
          var slideno = $(this).data('slide');
          $('.section__slide').slick('slickGoTo', slideno - 1);
        });

       //click to detail
       var $itemslide = $('.icon_slide .icon_slide__item');
       var $itemfooter = $('.section__2__footer__js ul li');
       var $section__2__popup = $('.section__2__popup');
       var $section__2__opacity = $('.section__2__opacity');
       var $section__2__popup__wrap = $('.section__2__popup__wrap');

       $(".icon_slide__item, .section__2__footer__js li").click(function() {
         $section__2__popup.hide();
         $section__2__opacity.addClass('off');
       });

       $section__2__popup__wrap.click(function() {
         $itemfooter.removeClass('current');
         $section__2__popup.fadeOut();
         $section__2__opacity.removeClass('off');
       });

       $(".icon_slide__item__1, .itemfooter1").click(function() {
         $itemfooter.removeClass('current');
         $('.section__2__popup__1').fadeIn();
         //$('.itemfooter1').addClass('current');
       });
       $(".icon_slide__item__2, .itemfooter2").click(function() {
         $itemfooter.removeClass('current');
         $('.section__2__popup__2').fadeIn();
          //$('.itemfooter2').addClass('current');
       });
       $(".icon_slide__item__3, .itemfooter3").click(function() {
         $itemfooter.removeClass('current');
         $('.section__2__popup__3').fadeIn();
        //  $('.itemfooter3').addClass('current');
       });
       $(".icon_slide__item__4, .itemfooter4").click(function() {
         $itemfooter.removeClass('current');
         $('.section__2__popup__4').fadeIn();
          //$('.itemfooter4').addClass('current');
       });
       $(".icon_slide__item__5, .itemfooter5").click(function() {
         $itemfooter.removeClass('current');
         $('.section__2__popup__5').fadeIn();
        //  $('.itemfooter5').addClass('current');
       });
       $(".icon_slide__item__6, .itemfooter6").click(function() {
         $itemfooter.removeClass('current');
         $('.section__2__popup__6').fadeIn();
          //$('.itemfooter6').addClass('current');
       });

});
//

$('.mainfoto .foto__leader').click(function (){
  $('.mainfoto li').removeClass('active');
  $(this).parent().addClass('active');
}),

$('.foto__close').click(function (){
  $('.mainfoto li').removeClass('active');
}),

//scrolldown.
$(window).scroll(function () {
  var wheight = $(window).scrollTop();
  var hheight = $('.header').outerHeight();

  // scroll header
  if(wheight > hheight + 20) {
      $(".header").addClass("active");
  } else {
      $(".header").removeClass("active")
  }
});


/*start:youtube embed*/
var tag = document.createElement('script');
tag.src = "http://www.youtube.com/player_api";

var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
var playerb;
function onYouTubePlayerAPIReady() {
  player = new YT.Player('ytplayer', {
    playerVars: {
      'autoplay': 0,
      'controls': 1,
      'autohide':1,
      'wmode':'opaque',
      'showinfo': 0,
      'loop': 1,
      'playlist': 'nTZ4GoETkyE',
      'rel': 0,
    },
    videoId: 'nTZ4GoETkyE',
    events: {
      'onReady': onPlayerReady
    }
  });

  playerb = new YT.Player('ytplayerb', {
    playerVars: {
      'autoplay': 0,
      'controls': 1,
      'autohide':1,
      'wmode':'opaque',
      'showinfo': 0,
      'loop': 1,
      'playlist': 'ZGbPWPmAXHs',
      'rel': 0,
    },
    videoId: 'ZGbPWPmAXHs',
    events: {
      'onReady': onPlayerReady
    }
  });


}

function onPlayerReady(event) {
      //event.target.mute();
      playerb.setVolume(0);
      //playerb.playVideo();
}
function stopVideo() {

}

function comingsoon() {
  alert("coming soon");
}
